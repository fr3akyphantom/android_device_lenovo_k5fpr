Copyright (C) 2018 MohanCM <mohanmanjappa@gmail.com>
<br>
Copyright (C) 2019 Sahil Sonar <sss.sonar2003@outlook.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Device Tree For Lenovo K4 Note
===================================== 

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa 1.3 GHz Cortex-A53
CHIPSET | Mediatek MT6753
GPU     | Mali-T720
Memory  | 2GB/3GB RAM
Android | 5.1.1 (up to 6.0)
Storage | 16 GB/32 GB
Battery | 3300 mAh
Display | 5.5" 1920 x 1080 px DPI 403
Rear Camera | 13MP, Int.13MP (Samsung S5K3M2)
Front Camera | 5MP, Int. 5MP (OV5693)

![Lenovo K4 Note](https://st1.bgr.in/wp-content/uploads/2015/12/7a4b4a4422506c905cd457d8a356233f_375x500_1.jpg)

This branch is for building LineageOS 15.1

Model Supported : A7010a48 A7010
